//
//  Cocktail.swift
//  CIMobile
//
//  Created by Timoté Vannier on 30/01/2023.
//

import Foundation

struct Cocktail: Codable {
    let id: Int
    var name: String
    let price: Double
    let alcohol: String
    let ingredients: String
    let description: String
}
