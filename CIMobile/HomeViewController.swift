//
//  HomeViewController.swift
//  CIMobile
//
//  Created by Timoté Vannier on 30/01/2023.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var cocktails: [Cocktail] = []
    @IBOutlet weak var cocktailsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UI CollectionView
        // Set the first item to the middle
        let cellWidth = 330.0
        let cellHeight = 400.0
        let insetX = (view.bounds.width - cellWidth) / 2.0
        let insetY = 0.0
        
        let layout = cocktailsCollectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        cocktailsCollectionView.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)
        
        
        self.cocktailsCollectionView.dataSource = self
        self.cocktailsCollectionView.delegate = self
        self.cocktailsCollectionView.register(UINib(nibName: "CocktailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CocktailCollectionViewCell")

        CocktailServices.getCocktails() { err, cocktails in
            guard err == nil else {
                print("ERROR: \(err)")
                // Alert message ?? si j'ai le temps
                return
            }
            
            print(cocktails)
            
            DispatchQueue.main.async {
                self.cocktails = cocktails!
                self.cocktailsCollectionView.reloadData()
            }
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cocktails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.cocktailsCollectionView.dequeueReusableCell(withReuseIdentifier: "CocktailCollectionViewCell", for: indexPath)
        return cocktailCollectionViewCell(cell: cell as! CocktailCollectionViewCell, index: indexPath.row)
    }
    
    
    func cocktailCollectionViewCell(cell: CocktailCollectionViewCell, index: Int) -> UICollectionViewCell {
        let cell = cell
        
        cell.setLabels(name: cocktails[index].name, description: cocktails[index].description)
        
        return cell
    }


}


// It will scroll to the next item if we release in the middle of two items
extension HomeViewController: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let layout = self.cocktailsCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpaces = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpaces
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpaces - scrollView.contentInset.left, y: scrollView.contentInset.top)
        
        targetContentOffset.pointee = offset
    }
    
}
