//
//  CocktailServices.swift
//  CIMobile
//
//  Created by Timoté Vannier on 30/01/2023.
//

import Foundation

class CocktailServices {
    
    class func getCocktails(completion: @escaping (Error?, [Cocktail]?) -> Void ) {
        let request = WebRequest.get(url: "/cocktails/")
        
        let task = URLSession.shared.dataTask(with: request) { data, res, err in
            guard let d = data else {
                completion(NSError(domain: "com.timdev.CIMobile", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "No data found"
                ]), nil)
                return
            }
            
            
            do {
                let obj = try JSONDecoder().decode([Cocktail].self, from: d)
                
                completion(nil, obj)
            } catch let err {
                completion(err, nil)
                return
            }
            
        }
        task.resume()
        
        
    }
    
    
    class func getCocktailById(id: Int, completion: @escaping (Error?, Cocktail?) -> Void ) {
        let request = WebRequest.get(url: "/cocktails/\(id)")
        
        let task = URLSession.shared.dataTask(with: request) { data, res, err in
            guard let d = data else {
                completion(NSError(domain: "com.timdev.CIMobile", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "No data found"
                ]), nil)
                return
            }
                        
            do {
                let obj = try JSONDecoder().decode([Cocktail].self, from: d)
                
                completion(nil, obj[0])
            } catch let err {
                completion(err, nil)
                return
            }
            
        }
        task.resume()
        
        
    }
    
    
    
    class func updateCocktail(id: Int, name: String, completion: @escaping (Error?, Bool?) -> Void) {
        
        let json: [String: Any] = [
            "name": name
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        
        let request = WebRequest.put(url: "/cocktails/\(id)", body: jsonData)
        
        let task = URLSession.shared.dataTask(with: request) { data, res, err in
            guard let d = data else {
                completion(NSError(domain: "com.timdev.CIMobile", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "No data found"
                ]), nil)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: d, options: .allowFragments)
                print("JSON: \(json)")
                completion(nil, true)
            } catch let err {
                completion(err, nil)
                return
            }
            
        }
        task.resume()
    }
    
    
    class func deleteCocktail(id: Int, completion: @escaping (Error?) -> Void) {
        let request = WebRequest.delete(url: "/cocktails/\(id)")
        
        let task = URLSession.shared.dataTask(with: request) { data, res, err in
            guard let d = data else {
                completion(NSError(domain: "com.timdev.CIMobile", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "No data found"
                ]))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: d, options: .allowFragments)
                print("JSON: \(json)")
                
                completion(nil)
            } catch let err {
                completion(err)
                return
            }
            
        }
        task.resume()
    }
    
}
