//
//  WebRequest.swift
//  CIMobile
//
//  Created by Timoté Vannier on 30/01/2023.
//

import Foundation

class WebRequest {
    
    static let base = "http://localhost:3000"
    
    
    class func get(url: String) -> URLRequest {
        var request = URLRequest(url: URL(string: base + url)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        return request
    }
    
    
    
    
    class func post(url: String, body: Data?) -> URLRequest {
        var request = URLRequest(url: URL(string: base + url)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        request.httpBody = body
        
        return request
    }

    
    class func put(url: String, body: Data?) -> URLRequest {
        var request = URLRequest(url: URL(string: base + url)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PUT"
        
        request.httpBody = body
        
        return request
    }
    
    class func delete(url: String) -> URLRequest {
        var request = URLRequest(url: URL(string: base + url)!)
        request.httpMethod = "DELETE"
        
        return request
    }
    
    
}
