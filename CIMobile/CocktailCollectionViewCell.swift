//
//  CocktailCollectionViewCell.swift
//  CIMobile
//
//  Created by Timoté Vannier on 30/01/2023.
//

import UIKit

class CocktailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameUILabel: UILabel!
    @IBOutlet weak var descriptionUILabel: UILabel!
    
    var name: String!
    var descriptionCocktail: String!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        layer.cornerRadius = 25
    }
    
    func setLabels(name: String, description: String) {
        self.name = name
        self.nameUILabel.text = name
        
        self.name = description
        self.descriptionUILabel.text = description
        
    }

}
